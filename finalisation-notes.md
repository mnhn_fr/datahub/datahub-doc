## Finalistaion projet Datahub


# Transférer sur Gitlab mnhn tous les livrables tech

=> Objectif : migrer tout ce qui est nécessaire au reboot du projet
* connecteurs, index, 
  * scripts de connecteurs graphe
    * scala
    * RML
  * script d'indexation

* datahub-models => ontologie et dérivés
  * requêtes SPARQL exemple
  * règles inférence
  * SHACL patterns

* datahub-doc => analyse + données 
  * script
  * SPARQL example
  * données test
  * liens vers dumps de data 

# Documentation sur reconstruction du graphe

* Reconstruction à partir
  * des données sources
  * des dumps RDF
* comment réindexer 

# Backlog mnhn:Person

* Retrouver les ID wikidata (auj ~1900) à partir
  * endpoint IDREF
  * enpoint Wikidata

* Stats sur distribution des personnes
  * Combien de personnes distinctes en excluant les doublons déjà alignés
  * Combien de personnes alignées

* Distribution des personnes reliées à d'autres référentiels
    * combien ont un ID ext
    * combien ont d'IDREF
    * combien Orcid, etc

* Vérifier qu'on a un foaf:name sur 

## références - notes

* Requête pour distribution des personnes sur réf ext
  
```sparql
# Requête pour distribution des ID ext des mnhn:Person
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
PREFIX mnhn: <https://www.data.mnhn.fr/ontology/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
select 
(count(?p) as ?ntotal)
(count(?wik) as ?nwikidata) 
(count(?idref) as ?nidref) 
(count(?bnf) as ?nbnf)
(count(?orcid) as ?norcid)
(count(?viaf) as ?nviaf)
(count(?isni) as ?nisni)
(count(?paligned) as ?naligned)

where {
    ?p a mnhn:Person.
    optional{?p mnhn:wikidataURI ?wik}
    optional{?p mnhn:idrefURI ?idref}
    optional{?p mnhn:bnfURI ?bnf}
    optional{?p mnhn:orcidURI ?orcid}
    optional{?p mnhn:isniURI ?isni}
    optional{?p mnhn:viafURI ?viaf}
    optional{
        ?p mnx:hasAlignement/mnx:alignedEntity ?paligned
    	}
    
} 
```

