# Test chargement méthode rapide GraphDB sur infra MNHN


## Test 2024.04.02

Sur node 

Avec données 

Bucket monté directement comme volume des 2 environnements "Import" et "Run"


```bash 

$ poc-graphdb-import-668cb598db-5xxv5:/data/import# /opt/graphdb/dist/bin/importrdf load -c mvp-datahub-config.ttl -m parallel mnhn.ttl
$ poc-graphdb-import-668cb598db-5xxv5:/data/import# ls /opt/graphdb/home/data/repositories/
mvp-datahub                  mvp-datahub-springy-morning  preprod-datahub
$ poc-graphdb-import-668cb598db-5xxv5:/data/import# /opt/graphdb/dist/bin/importrdf load -f -i mvp-datahub -m parallel 
```


## Commandes Kub pour lancer environnements import et run de GDB

```bash  

# SCALE DOWN GDB instance
kubectl -n mnhn scale sts graphdb-node --replicas 0

# SCALE UP IMPORT DEPLOYMENT
kubectl -n mnhn scale deploy poc-graphdb-import --replicas 1

# -----

# SCALE DOWN IMPORT DEPLOYMENT
kubectl -n mnhn scale deploy poc-graphdb-import --replicas 0

# SCALE UP GDB instance
kubectl -n mnhn scale sts graphdb-node --replicas 1

```