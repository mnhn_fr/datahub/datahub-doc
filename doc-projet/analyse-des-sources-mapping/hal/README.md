## HAL 

Les données HAL peuvent être retrouvé via l'api rest HAL du MNHM, ou via le protocol OAI-PMH, ou via un sparql Endpoint.

- Connecteur OAI-PMH déjà implémenté dans Datapoc2
- Périmètre MVP : article lié au MNHN ~12700
- TODO  : trouver moyen de récup les fichiers plein texte




### API Solr 

* Doc : https://api.archives-ouvertes.fr/docs/search 
* API principale de HAL
* Basé sur l'API du moteur de recherche Apache SOLr utilisé par la plateforme HAL. Permet donc d'utiliser la syntaxe et les fonctions de recherche et agrégation proposée par Solr, dont la compatibilité avec la syntaxe Lucene par exemple (https://solr.apache.org/guide/6_6/the-standard-query-parser.html#the-standard-query-parser)

Le portail lié au MNHM est :
```json
{
    "id": "34",
    "code": "mnhn",
    "name": "MNHN - Muséum national d'Histoire naturelle",
    "url": "https://hal-mnhn.archives-ouvertes.fr"
}
```
On peut s'inspirer de https://bitbucket.org/ucagate/ucagate-backend/src/master/modules/connectors/src/main/scala/org/ucagate/connectors/hal/helpers/HALRestClient.scala pour le client HTTP et https://bitbucket.org/ucagate/ucagate-backend/src/master/modules/connectors/src/main/scala/org/ucagate/connectors/hal/models/HALDocument.scala pour le model renvoyé.

#### Exemples de requêtes

* Articles liés au MNHN, recherche de "solanaceae" dans tous les champs textes :
https://api.archives-ouvertes.fr/search/?q=text_fulltext:solanaceae&fq=instStructAcronym_s:MNHN&wt=json

* Usage du filtre openaccess pour articles MNHN https://api.archives-ouvertes.fr/search/?q=*%3A*&fq=instStructAcronym_s:MNHN&fq=openAccess_bool:true&wt=json

* idem avec liste des champs retournés : https://api.archives-ouvertes.fr/search/?q=*%3A*&fq=instStructAcronym_s:MNHN&fq=openAccess_bool:true&wt=json&fl=title_s,releasedDate_tdate,uri_s,openAccess_bool,abstract_s,fileMain_s,label_s, Résultats = hal/hal-result_mnhn_openaccess.json

* articles en Science du Vivant (catégorie)
https://api.archives-ouvertes.fr/search/?q=*%3A*&wt=xml-tei&fq=level0_domain_s:sdv

#### moissonnage incrémental

Inclure le modified date dans la requête : ex. pour les docs MNHN modifiés à partir d'auj, 7 nov 2023:

https://api.archives-ouvertes.fr/search/?q=*%3A*&fq=instStructAcronym_s:MNHN&wt=xml-tei&fq=modifiedDate_s:[2023-11-07%20TO%20*]


#### Recherche plein texte VS. récupération des pdf

Champs pertinents :
* fulltext_t = indexé uniquement; texte pour recherche/filtrage
* files_s = lien des fichiers pdf tels que déposés 
* fileMain_s : lien vers le fichier pdf parsé comme étant le principal

2 stratégies complémentaires / combinables:

1. utiliser pour l'analyse l'index, via un filtre de requête sur fulltext_t, pour récuperer directement les docs contenant une chaine de caractère ou un pattern donné, e.g en exploitant les capacités du language de règles/opérateurs logiques de SOlr (compatible Lucene) 
2. récupérer les liens de tous les fichiers PDF disponibles dans HAL (avec ou sans filtrage, cf 1.). Pour identifier les docs HAL  ayant un lien vers le PDF, on peut utiliser un filtre de requête sur fulltext_t `&fq=fulltext_t:{*%20TO%20*}`, ce qui donnerait e.g la requête suivante:

https://api.archives-ouvertes.fr/search/?q=*%3A*&fq=instStructAcronym_s:MNHN&fq=fulltext_t:{*%20TO%20*}&fq=openAccess_bool:true&wt=json&fl=title_s,releasedDate_tdate,uri_s,openAccess_bool,abstract_s,fileMain_s,file_s,halId_s,docid,domain_s,files_s

**Cette requête nous apprend qu'il y a 12153 doc HAL dont le full texte est indexé et donc pour lesquels on peut récupérer le fichier PDF**


#### Mapping champ dublin-core - API Solr

* dc:publisher => publisher_s
* dc:title => title_s
* dc:creator => authUrl_s, authQuality_s, authFirstName_s, authMiddleName_s, authLastName_s,  authIdFormPerson_s, authFullNameIdHal_fs, authFullNameId_fs, authIdFullName_fs, authIdHal_s, authOrganism_s
* dc:contributor => idem dc:creator
* dc:description => description_s, abstract_s
* dc:source => source_s
* dc:identifier => docid (non vide), halId_s 
* dc:relation => ?
* dc:language => language_s
* dc:subject => domain_s, domainAllCode_s
* dc:type => docType_s
* dc:date => releasedDate_tdate
* dc:rights => licence_s

fl=publisher_s,title_s,authUrl_s,authQuality_s,authFirstName_s,authMiddleName_s,authLastName_s,authIdFormPerson_s,authFullNameIdHal_fs,authFullNameId_fs,authIdFullName_fs,authIdHal_s,authOrganism_s,description_s,source_s,docid,halId_s,language_s,domain_s,domainAllCode_s,docType_s,releasedDate_tdate,licence_s,fileMain_s,uri_s

Requête complète :
https://api.archives-ouvertes.fr/search/?q=*%3A*&fq=instStructAcronym_s:MNHN&fq=fulltext_t:{*%20TO%20*}&fq=openAccess_bool:true&wt=json&fl=publisher_s,title_s,authUrl_s,authQuality_s,authFirstName_s,authMiddleName_s,authLastName_s,authIdFormPerson_s,authFullNameIdHal_fs,authFullNameId_fs,authIdFullName_fs,authIdHal_s,authOrganism_s,description_s,source_s,docid,halId_s,language_s,domain_s,domainAllCode_s,docType_s,releasedDate_tdate,licence_s,fileMain_s,uri_s

Echantillon de résultat : 
[hal-result_mnhn_openaccess-fullOutput.json](hal-result_mnhn_openaccess-fullOutput.json)

### API OAI-PMH


Nous pouvons aussi requêter l'API OAI-PMH en utilisant le set "collection:MEM-MNHN" ou le set "collection:MNHN".

* endpoint (base url) = https://api.archives-ouvertes.fr/oai/hal/ 
* ListIdentifiers = https://api.archives-ouvertes.fr/oai/hal/?verb=ListIdentifiers&metadataPrefix=oai_dc&set=collection:MNHN 
* ListRecords = https://api.archives-ouvertes.fr/oai/hal/?verb=ListRecords&metadataPrefix=oai_dc&set=collection:MNHN 
* GetRecord (détails sur 1 record) = https://api.archives-ouvertes.fr/oai/hal/?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:HAL:hal-03523458v1 
* List formats https://api.archives-ouvertes.fr/oai/hal/?verb=ListMetadataFormats 

**Format disponible**:

* Dubline Core : `oai_dc`
* DC Terms : `oai_dcterms`
* OpenAire : `oai_opeaire`
* XML TEI : `xml-tei`


#### OAI-PMH - encodage XML-TEI

La structure et le contenu des réponses au format XML-TEI est beaucoup plus riche que tous les autres formats.
* les fichiers sont structurés en 2 parties, une listant les entrées
`body` et une autre `back` listant des infos mutualisés entre les entrées comme les structures auxquelles sont affiliés les personnes
* les infos sur les auteurs sont bcp plus riches et plus facile à parser dans l'optique de générer du RDF :

```xml
<titleStmt>
    <title xml:lang="fr">La forêt : refuge pour les hommes, les chimpanzés ou les esprits ?</title>
    <author role="aut">
        <persName>
            <forename type="first">Sarah</forename>
            <surname>Bortolamiol</surname>
        </persName>
        <email type="md5">9c564b87aac5e3f5fd7aaa09ed90481f</email>
        <email type="domain">cnrs.fr</email>
        <idno type="idhal" notation="string">sarah-bortolamiol</idno>
        <idno type="idhal" notation="numeric">742818</idno>
        <idno type="halauthorid" notation="string">22884-742818</idno>
        <idno type="IDREF">https://www.idref.fr/183536002</idno>
        <idno type="ORCID">https://orcid.org/0000-0002-9731-2777</idno>
        <idno type="GOOGLE SCHOLAR">https://scholar.google.com/citations?user=sBF_BzMAAAAJ&hl=en</idno>
        <orgName ref="#struct-441569" />
        <affiliation ref="#struct-1096" />
        <affiliation ref="#struct-117740" />
    </author>
    <author role="aut">
        <persName>
            <forename type="first">Marianne</forename>
            <surname>Cohen</surname>
        </persName>
        <email type="md5">431e873a571533afb46d7611313b93fb</email>
        <email type="domain">paris-sorbonne.fr</email>
        <idno type="idhal" notation="numeric">968805</idno>
        <idno type="halauthorid" notation="string">930336-968805</idno>
        <orgName ref="#struct-123821" />
        <affiliation ref="#struct-224523" />
    </author>
    <author role="aut">
        <persName>
            <forename type="first">Sabrina</forename>
            <surname>Krief</surname>
        </persName>
        <email type="md5">ee1b57d2ad21d83be52119fe78457d3e</email>
        <email type="domain">mnhn.fr</email>
        <idno type="idhal" notation="string">sabrina-krief</idno>
        <idno type="idhal" notation="numeric">177674</idno>
        <idno type="halauthorid" notation="string">1038-177674</idno>
        <idno type="IDREF">https://www.idref.fr/07628610X</idno>
        <affiliation ref="#struct-117740" />
    </author>
    ```

* les balises ` <affiliation ref="#struct-1096" />` renvoient donc à la 2ème partie du fichier XML réponse `back` ou les infos des structures sont détaillées:

```xml
<back>
    <listOrg type="structures">
        <org type="institution" xml:id="struct-7512" status="VALID">
            <orgName>Muséum national d'Histoire naturelle</orgName>
            <orgName type="acronym">MNHN</orgName>
            <desc>
                <address>
                    <addrLine>57, rue Cuvier - 75231 Paris Cedex 05</addrLine>
                    <country key="FR" />
                </address>
                <ref type="url">http://www.mnhn.fr</ref>
            </desc>
        </org>
        <org type="institution" xml:id="struct-301475" status="VALID">
            <orgName>Institut national de recherches archéologiques préventives</orgName>
            <orgName type="acronym">Inrap</orgName>
            <desc>
                <address>
                    <addrLine>121 rue d'Alésia75014 Paris </addrLine>
                    <country key="FR" />
                </address>
                <ref type="url">http://www.inrap.fr/</ref>
            </desc>
        </org>
```


#### OAI-PMH - Encodage DublinCore

Extrait du fichier hal/hal-03523458v1_dublincore.xml

```xml
<oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/  http://www.openarchives.org/OAI/2.0/oai_dc.xsd http://purl.org/dc/elements/1.1/  http://dublincore.org/schemas/xmls/qdc/2008/02/11/dc.xsd">
    <dc:publisher>HAL CCSD</dc:publisher>
    <dc:title xml:lang="en">Building a new path: from materiality to ecological transition in training</dc:title>
    <dc:creator>Fayol, Noémie</dc:creator>
    <dc:creator>Cerceau, Juliette</dc:creator>
    <dc:contributor>IMT Mines Alès - ERT (ERT) ; IMT - MINES ALES (IMT - MINES ALES) ; Institut Mines-Télécom [Paris]
        (IMT)-Institut Mines-Télécom [Paris] (IMT)</dc:contributor>
    <dc:contributor>PEnSTer: Pollutions Environnement Santé Territoire (PEnSTer) ; Hydrosciences Montpellier (HSM) ;
        Institut national des sciences de l'Univers (INSU - CNRS)-Institut de Recherche pour le Développement
        (IRD)-Université Montpellier 2 - Sciences et Techniques (UM2)-Université de Montpellier (UM)-Centre National de
        la Recherche Scientifique (CNRS)-Institut national des sciences de l'Univers (INSU - CNRS)-Institut de Recherche
        pour le Développement (IRD)-Université Montpellier 2 - Sciences et Techniques (UM2)-Université de Montpellier
        (UM)-Centre National de la Recherche Scientifique (CNRS)</dc:contributor>
    <dc:description>National audience</dc:description>
    <dc:source>DEEPSURF Conference 2021</dc:source>
    <dc:coverage>Nancy, France</dc:coverage>
    <dc:identifier>hal-03523458</dc:identifier>
    <dc:identifier>https://hal.mines-ales.fr/hal-03523458</dc:identifier>
    <dc:source>https://hal.mines-ales.fr/hal-03523458</dc:source>
    <dc:source>DEEPSURF Conference 2021, Oct 2021, Nancy, France</dc:source>
    <dc:language>en</dc:language>
    <dc:subject>[SPI]Engineering Sciences [physics]</dc:subject>
    <dc:type>info:eu-repo/semantics/conferenceObject</dc:type>
    <dc:type>Conference papers</dc:type>
    <dc:date>2021-10-12</dc:date>
</oai_dc:dc>
```

#### OAI-PMH/DC - Récupérer les entrées en open access

* les fichiers en open access ont `<dc:rights>info:eu-repo/semantics/OpenAccess</dc:rights>`
* pour ces fichiers open access, un des `dc:identifier` a une URL vers un fichier pdf

```xml
 <dc:identifier>meteo-00996738</dc:identifier>
  <dc:identifier>https://hal-meteofrance.archives-ouvertes.fr/meteo-00996738</dc:identifier>
  <dc:identifier>https://hal-meteofrance.archives-ouvertes.fr/meteo-00996738/document</dc:identifier>
  <dc:identifier>https://hal-meteofrance.archives-ouvertes.fr/meteo-00996738/file/joly_et_al_2007_accepted.pdf</dc:identifier>
  <dc:source>https://hal-meteofrance.archives-ouvertes.fr/meteo-00996738</dc:source>
```

#### OAI-PMH : moissonnage incrémental  (ou différentiel) avec 'from='

L'idée ici est de ne moissonner que ce qui a changé depuis le dernier moissonnage.
Avec le protocol OAI-PMH, on peut utiliser le paramètre de requête "from=date-la-plus-ancienne-souhaitée". 

Par exemple     
`view-source:https://api.archives-ouvertes.fr/oai/hal/?verb=ListRecords&metadataPrefix=oai_dc&set=collection:MNHN&from=2021-06-01`      
pour ne récupérer que les entrées modifiées après le 1er juin 2021.

Sur le jeu de données MNHN, si on regarde les entrées modifiées depuis la veille (nous sommes le 6 nov 2023):       
`view-source:https://api.archives-ouvertes.fr/oai/hal/?verb=ListRecords&metadataPrefix=oai_dc&set=collection:MNHN&from=2023-11-05`      
on récupère 2335 entrées nouvellement modifiées/ajoutées depuis hier.


### SPARQL Endpoint

Il existe aussi un [sparql Endpoint](http://sparql.archives-ouvertes.fr/sparql) : http://sparql.archives-ouvertes.fr/sparql

SPARQL endpoint ne contient pas tous les articles (cf [requête](http://sparql.archives-ouvertes.fr/sparql?default-graph-uri=&query=SELECT+%28count%28%3Fa%29+as+%3Fnb%29%0D%0Awhere%7B%0D%0A%3Fa+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2FisPartOf%3E+%3Chttps%3A%2F%2Fhal-mnhn.archives-ouvertes.fr%2FMNHN%3E%7D&format=text%2Fhtml&timeout=0&debug=on&run=+Run+Query+)) seulement 2120 sur les 12700 




