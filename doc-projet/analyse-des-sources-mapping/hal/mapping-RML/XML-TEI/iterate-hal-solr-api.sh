#!/bin/bash
# create out dir if not exist
mkdir -p output-files

# iterate 
for (( i=0; i<=197469; i+=1000 ))
do
    echo 'Iterating : i= '$i
    sed -r 's/\&start=[0-9]+/\&start='$i'/g' hal-xmltei-to-rdf.rml.ttl > rmlmap.ttl
    java -jar ~/vendors/rmlmapper-latest.jar -m rmlmap.ttl -o output-files/hal-to-rdf_$i.out.trig -s trig 
    read -t 10 # wait 10 seconds
done
mv rmlmap.ttl

echo 'compressing to HAL.trig.zip'
zip -r  HAL.trig.zip output-files/*