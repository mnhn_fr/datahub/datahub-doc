#!/bin/bash
# create out dir if not exist
mkdir -p outrdf
date +%H:%M:%S 
# iterate on files in inpout folder
for filename in input-files/xml/*.xml; 
do
    echo '******* Iterating : filename= '$filename
    file=$(echo "$filename" | sed -e 's~input-files\/xml\/~~g') 
    echo 'file = '$file 
    sed -r 's~input-files\/1000.xml~input-files/xml/'$file'~g' hal-oai-dc-to-rdf4.rml.ttl > rmlmap.ttl
    java -jar ~/src/rml-tuto/rmlmapper.jar -m rmlmap.ttl -o outrdf/$file.out.trig -s trig
done
date +%H:%M:%S 
#
# "AoElOTczMjg=" "AoEmOTQwOTE5" "AoEmOTE2MDQx" "AoElODc4ODc=" "AoElODY3MzM=" "AoEmODQ4Nzk1" "AoEmODIxNDQ3" "AoElNzg4MzQ=" "AoEmNzY0MTQ4" "AoEmNzU1ODk1" "AoEmNzM0Mzkw" "AoEmNzIxMjky"
#rml-tuto/rmlmapper.jar
#oai_dc_file00.xml