Mapping RDF - Hal - API SOlr - format XML-TEI
==============================================


# Requêtes


Le scope:

* API HAL Solr : https://api.archives-ouvertes.fr/docs/search 
* Articles liés aux siences du vivant, donc ayant le domaine `sdv`
* Format: XML-TEI car le mieux structuré et donnant le plus d'info sur les auteurs et les structures 
* => 723 304 articles (au 15.11.23)

* Exemple de requêtes : 
  * https://api.archives-ouvertes.fr/search/?q=*%3A*&wt=xml-tei&fq=openAccess_bool:true&fq=level0_domain_s:sdv
  * Fichier résultat correspondant : sample-hal_sdv_xml-tei_30.xml

# Structure du XML-TEI

* <teiHeader> 
  * metadonnée
  * nb total articles sur le résultat de la requête
  * nb art dans document
* <body>
  * <listBibl> la liste des articles <biblFull>
  * chaque bibFull a 
    * <titleStmt> titre et info sur dépositeur et auteurs
    * <editionStmt> date de soumission, creation, etc + lien vers fichiers  si dispo
    * <publicationStmt> id, URI, réference texte
    *  <seriesStmt> info sur affiliations
    *  <notesStmt> audience, categorie de publi
    *  <sourceDesc> 
       *  détails sur auteurs
       *  id du journal, date, vol, issue etc.
       *  DOI et id pubmed, etc
    *  <profileDesc> classifications et abstract
* <back> reprend les infos détaillés des affiliations (listOrg) des auteurs des entrées du <body> 

# Mapping RDF


