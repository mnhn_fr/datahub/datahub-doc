# Base moleculaire

* doc api : http://datahub-test-2.arzt.mnhn.fr:8092/v3/api-docs


## Moissonage

1. moissoner http://datahub-test-2.arzt.mnhn.fr:8092/specimens

```json 
{
    "id": 27,
    "collectionCode": "IM",
    "catalogNumber": "2007-32544",
    "sampleId": "MNHN-IM-2007-32544",
    "boldProcessId": "NEOGA027-10",
    "boldCreationDate": "2010-11-29",
    "boldReturnDate": "2010-11-30",
    "created": "2010-11-29T16:21:12.000+00:00",
    "_links": {
        "self": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/specimens/27"
        },
        "bold": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/specimens/27/bold"
        },
        "samples": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/samples?collectionCode=IM&catalogNumber=2007-32544"
        },
        "sequences": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/sequences?collectionCode=IM&catalogNumber=2007-32544{&sampleId}",
            "templated": true
        }
    }
}
```

=>  on récupère :   
* la liste des liens vers les "**séquences**" e.g `http://datahub-test-2.arzt.mnhn.fr:8092/sequences?collectionCode=IM&catalogNumber=2007-32544{&sampleId}` ((rmq: il faudrait au passage virer les {&sampleId} ))
  * from /_links/sequences/href
* la liste des liens **"bold"** e.g  `http://datahub-test-2.arzt.mnhn.fr:8092/specimens/27/bold` 
  * from /_links/bold/href

2. **Pour les séquences GenBank** => on déroule les liens "séquences", eg: http://datahub-test-2.arzt.mnhn.fr:8092/sequences?collectionCode=IM&catalogNumber=2007-32544, en extrayant les paramètres, ie
   
   * collectionCode =
   * catalogNumber=

ce qui nous permet de raccrocher le specimen de la BU. Chacune de ces pages  contient une liste de séquences, eg:
   
```json 
{
"id": 2350,
"gene": "COI",
"madeByInstitution": "MNHN",
"genbankAccessionNumber": "KT753908=JQ950196",
"urlFasta": "marbol/MNHN-2009-06-29-MARBOL-31/fasta/Marbol31_H11_32544_COI.Fasta",
"boldPublicationDate": "2012-02-29T23:00:00.000+00:00",
"boldPublication": "O",
"created": "2009-09-27T15:03:13.000+00:00",
"modified": "2020-04-09T11:37:24.000+00:00",
"_links": {
    "self": {
    "href": "http://datahub-test-2.arzt.mnhn.fr:8092/sequences/2350"
    },
    "GenBank": {
    "href": "https://www.ncbi.nlm.nih.gov/nuccore/KT753908=JQ950196"
    }
}
}

```

avec optionnellement : 
   * nom de gène "gene" , eg "COI"
   * un "genbankAccessionNumber", eg. "KT753908=JQ950196"
   * un lien GenBank, eg. "https://www.ncbi.nlm.nih.gov/nuccore/KT753908=JQ950196"


3. **Pour les séquences Bold**

ON déroule la liste des liens "bold" eg http://datahub-test-2.arzt.mnhn.fr:8092/specimens/26/bold

```json
{
  "processId": "NEOGA026-10",
  "boldRecords": [
    {
      "id": 1752628,
      "processId": "NEOGA026-10",
      "binUri": "BOLD:AAO3731",
      "sampleId": "MNHN-IM-2007-32539",
      "catalogNumber": "MNHN_IM200732539",
      "collectionCode": null,
      "institution": "Museum national d'Histoire naturelle",
      "fieldNumber": null,
      "sequences": [
        {
          "id": 3757465,
          "markerCode": "COI-5P",
          "genbankAccession": null,
          "nucleotides": "AACATTATATATTCTATTTGGTATATGATCAGGGCTAGTTGGTACTGCTCTTAGTCTTCTTATTCGAGCTGAATTAGGACAACCTGGGGCTCTATTAGGTGATGACCAACTATATAACGTTATTGTAACAGCTCATGCTTTTGTTATAATTTTTTTTTTAGTTATACCTATAATAATTGGAGGTTTTGGGAATTGGTTAGTTCCTTTAATGTTAGGAGCTCCAGATATAGCCTTTCCTCGTTTGAATAATATAAGATTTTGACTCTTACCTCCTGCTTTATTACTTTTACTTTCATCCGCTGCAGTTGAAAGAGGAGTAGGGACAGGATGAACTGTATATCCTCCTTTAGCAGGAAATTTAGCTCATGCTGGGGGTTCTGTTGATTTAGCTATTTTTTCTTTACATCTTGCTGGTGTTTCATCTATTTTAGGTGCTGTAAATTTTATCACAACTATTATCAATATACGATGACGTGGAATACAATTTGAGCGTCTTCCTTTATTTGTGTGATCAGTAAAAATTACTGCTATTTTACTTCTATTATCCTTGCCAGTTTTAGCTGGGGCTATTACTATGCTTTTAACTGATCGAAATTTTAATACTGCTTTCTTTGATCCAGCCGGAGGTGGAGATCCTATTTTATATCAGCATCTATTT"
        }
      ]
    }
  ],
  "_links": {
    "self": {
      "href": "http://datahub-test-2.arzt.mnhn.fr:8092/specimens/26/bold"
    },
    "bold": {
      "href": "http://v3.boldsystems.org/index.php/API_Public/combined?ids=NEOGA026-10"
    }
  }
}

```
Pour chaque JSON, on parse
* les `boldRecords` et le premier item de la liste `sequences`

Et on peuple les objets `mnhn:GenomicSequenceRecord` avec 

* l'id de chaque séquence `boldRecords[i]/sequences[1]/id nom du gène` 
* geneName` => from boldRecords[i]/sequences[1]/markerCode (e.g "COI-5P")
* lien bold => from /links/bold/href


## Mapping 

CF [mapping_moleculaire.rdf-cible.trig](mapping_moleculaire.rdf-cible.trig) pour le mapping RDF