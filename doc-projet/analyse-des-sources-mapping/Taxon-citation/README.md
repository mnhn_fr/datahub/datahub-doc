

# Input data

- 15 333 doc_id uniques
- (a priori) les 40k (?) taxons Taxref 


## Plazi 


```json
 
        "taxon":"Ouratea parvifolia",
        "biblio":"Sastre, Claude & Offroy, Bérangère, 2016, Révision nomenclaturale des binômes du genre néotropical Ouratea Aublet (Ochnaceae) décrits par Van Tieghem, Adansonia 38 (1), pp. 55-98  : 97-98",
        "citation":"97-98",
        "taxon_id":"4c388603-7933-5f9d-be83-9ab17fe4287e",
        "DOI":"10.5252\/a2016n1a5"
        // similarity score = 100% car déjà filtré à la source
    ,

```