#!/bin/bash
# create out dir if not exist
mkdir -p output
# iterate on resume tokens
declare -a mappings=("gbif" "taxref" "ipni" "worms" "msw")
for i in "${mappings[@]}"
do
    echo 'Iterating : i= '$i
    #sed -r 's/\&resumptionToken=[^"]+/\&resumptionToken='$i'/g' hal-oai-dc-to-rdf.rml.ttl > rmlmap.ttl
    java -jar ~/vendors/rmlmapper-latest.jar -m taxon-$i-mapping.rml.ttl -o ../output/taxon-mapping-$i.out.trig -s trig -v
done
