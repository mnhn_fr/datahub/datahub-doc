## GBIF


Le GBIF est une plateforme internationale agrégeant les données d'observations/decsription de specimens fournis par de nombreuses institutions ou observatoires naturalistes, ainsi que par certaines applications d'observations participatives (iNaturalist e.g)

Les données ne sont donc pas toutes forcément vérifiées, mais leur statut de vérification est indiqué à chaque fois.


### Connexion à l'API `occurence`

* URL de la page permettant d'explorer les données et de crééer un lien de téléchargement :
https://www.gbif.org/occurrence/search?hosting_organization_key=2cd829bb-b713-433d-99cf-64bef11e5b3e
* En raison de la taille de ces données, on doit passer par l'API de téléchargement :
https://www.gbif.org/developer/occurrence#download 
* un client java codé pour cette API:
 https://github.com/gbif/occurrence/tree/master/occurrence-ws-client
* Compte mnx sur gbif
    - login : mnemotix
    - mail: contact@mnemotix.com
    - mdp: mnxpowa!

En allant sur l'onglet "Download" du compte, on voit la liste des téléchargements disponibles. Par exemple [ce lien](https://www.gbif.org/occurrence/download/0104244-220831081235567) correspond au dataset Gbif-MNHN complet, avec plus 9,4 millions d'occurrences. Le lien de téléchargement du fichier zip, qui doit d'abord être généré de manière asynchrone, est le suivant:
https://api.gbif.org/v1/occurrence/download/request/0104244-220831081235567.zip 

#### Créer un nouveau téléchargement via le site gbif.org

Pour créer un nouveau fichier de téléchargement via le site web gbif.org il suffit de faire une recherche (avec eg. [ce lien](https://www.gbif.org/occurrence/search?hosting_organization_key=2cd829bb-b713-433d-99cf-64bef11e5b3e)) et de cliquer, une fois connecté, sur "download", et de choisir le format d'archive "Simple". Ce type d'archive contient un unique fichier CSV contenant les informations requises pour ajouter les occurrences dans le jeu de données Datapoc2.

Il n'est pas possible de mettre à jour un lien de téléchargement déjà généré avec les nouvelles données pour une requête donnée. Pour télécharger les nouvelles données, il faut recommencer le processus :
1. requête https://www.gbif.org/occurrence/search?hosting_organization_key=2cd829bb-b713-433d-99cf-64bef11e5b3e 
2. création d'une instance de téléchargement (on peut passer directement à cette étape) https://www.gbif.org/occurrence/download?hosting_organization_key=2cd829bb-b713-433d-99cf-64bef11e5b3e
3. ATTENDRE que le fichier de dl soit disponible (via l'API le statut est consultable, cf ci-dessous) via la page nouvellement créée de download, e.g  https://www.gbif.org/occurrence/download/0038275-220831081235567
4. récupérer le lien de téléchargement sur la page e.g https://api.gbif.org/v1/occurrence/download/request/0038275-220831081235567.zip  

#### Utiliser l'API pour la création du fichier de téléchargement

Le contenu ci-dessous est repris dans le fichier gibf-dl-query.json
 ```json

 {
  "creator": "mnemotix",
  "notificationAddresses": [
    "contact@mnemotix.com"
  ],
  "sendNotification": false,
  "format": "SIMPLE_CSV",
  "predicate": {
      "type": "equals",
      "key": "HOSTING_ORGANIZATION_KEY",
      "value": "2cd829bb-b713-433d-99cf-64bef11e5b3e",
      "matchCase": false
      }
  }
 ```

 Requête curl pour récupérer la clé de dl :
 ```
 curl --include --user mnemotix:mnxpowa! --header "Content-Type: application/json" --data @gbif-dl-query.json https://api.gbif.org/v1/occurrence/download/request
 ```

 On récupère en retour un ID de dl : `0196867-210914110416597`. On peut voir le statut du dl à cette URL:
 https://www.gbif.org/occurrence/download/0196867-210914110416597 
 ou en mode API ici:
 https://api.gbif.org/v1/occurrence/download/0196867-210914110416597    

Dans l'API le statut de l'instance de téléchargement ressemble à ce JSON:

```json
{
  "key": "0196867-210914110416597",
  "doi": "10.15468/dl.j5zx5g",
  "license": "http://creativecommons.org/licenses/by/4.0/legalcode",
  "request": {
    "predicate": {
      "type": "equals",
      "key": "HOSTING_ORGANIZATION_KEY",
      "value": "2cd829bb-b713-433d-99cf-64bef11e5b3e",
      "matchCase": false
    },
    "sendNotification": false,
    "format": "SIMPLE_CSV",
    "type": "OCCURRENCE",
    "verbatimExtensions": [
      
    ]
  },
  "created": "2022-03-29T10:07:35.913+00:00",
  "modified": "2022-03-29T10:14:32.531+00:00",
  "eraseAfter": "2022-09-29T10:07:35.839+00:00",
  "status": "SUCCEEDED",
  "downloadLink": "https://api.gbif.org/v1/occurrence/download/request/0196867-210914110416597.zip",
  "size": 707495060,
  "totalRecords": 9365615,
  "numberDatasets": 37
}
```