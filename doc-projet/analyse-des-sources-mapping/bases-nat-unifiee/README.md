# Bases Naturalistes Unifiées

Projet de regrouper en 1 seule  base unifiée SQL (PostGres) les données fournies par les collections naturalistes du MNHN, portant sur ~12 millions de specimens.

## Collections

Ces données sont en tout premier organisées en collections qui peuvent être regroupées selon 5 catégories:

* BOTANIQUE :  Cryptogames PC, Ethnobotanique PAT, Herbier du harmas de J. H. Fabre FABR, Jardins JB, Laboratoire Maritime de Concarneau CO, Laboratoire Maritime de Dinard DIN, Plantes vasculaires P
* TERRE ET HOMMES: Anthropologie HA, Ethnologie E, Géologie générale GG, Météorites GT, Minéraux MIN, Paléontologie F, Préhistoire HP, Sédiments et roches océaniques GS
* RESSOURCES BIOLOGIQUES: Chimiothèque CH, Cyanobactéries PMC, Eucaryotes unicellulaires CEU, Extractothèque CX, Microalgues eucaryotes ALCP, Souches Fongiques RF, Tissus et cellules de vertébrés TCCV
* ZOOLOGIE - INVERTÉBRÉS: Annélides, Polychètes et Sipunculides IA, Aptérygotes & Collemboles EA, Arachnides : Araneae AR, Arachnides : Scorpionidae RS, Bryozoaires Brachiopodes IB, Cnidaires IK, Crustacés IU, Échinodermes IE, Insectes : Coléoptères EC, Insectes : DiptèresED, Insectes : Hémiptères EH, Insectes : Hyménoptères EY, Insectes : Lépidoptères EL, Insectes : Orthoptéroïdes EO, Insectes : Petits ordres &Odonates EP, Mollusques IM, Myriapodes MY, Nématodes IN, Porifères IP, Protistes IR, Tuniciers/ascidies IT
* ZOOLOGIE - VERTEBRÉS : Ichtyologie IC, Mammifères ZM, Oiseaux ZO, Reptiles & Amphibiens RA, Vertébrés : Autres ZA

## Index ES

### Echantillonage

On a 11 index à traiter:
```
bu-collection-event
bu-collection-group
bu-collection
bu-coordinates
bu-domain
bu-geolocation
bu-identification
bu-institution
bu-material
bu-material-relationship
bu-taxon
```


Exemples pour l'index `dh_coordinates`:

1. récupère le mapping
```
curl -X GET 'http://192.168.32.24:9200/dh-coordinates/_mapping?pretty' > coordinates_mapping.json
```
1. recupère les 100 premiers doc de chaque index
```
curl -X GET 'http://192.168.32.24:9200/dh-coordinates/_search?size=100&pretty'  -H 'Content-Type: application/json' -d '
{
    "query" : {
        "match_all" : {}
    }
}' > coordinates_sample.json
```

### Requêtes ES

**Rechercher doc par match exact sur field de type keyword**

curl -X GET "http://192.168.32.24:9200/bu-material/_search?pretty" -H 'Content-Type: application/json' -d '{
  "query": {
        "match_phrase": {
          "material_id": "8deceaf2-028a-5ddc-82ba-eb82a52dc139"
        }
      }
}'

curl -X GET "http://192.168.32.24:9200/bu*/_search?pretty" -H 'Content-Type: application/json' -d '{
  "query": {
        "match_phrase": {
          "catalog_number" : "P04834977"
        }
      }
}'

curl -X GET "http://192.168.32.24:9200/bu-material*/_search?pretty" -H 'Content-Type: application/json' -d '{
  "query": {
        "match_phrase": {
          "material_id": "abc1ec5d-237b-5b2f-9998-2a66f93c3553"
        }
      }
}' 

Exemple de 3 `material_id` de material testés, 3 cas différents :

- abc1ec5d-237b-5b2f-9998-2a66f93c3553 => décrit mais pas lié dans GDB, absent de l'index
- 7489a244-3a22-5987-9440-ac885b54969d => lié, mais pas décrit dans GDB, présent dans l'index
- 8deceaf2-028a-5ddc-82ba-eb82a52dc139 => lié, mais pas décrit dans GDB, présent dans l'index



### Liste des indexes

Depuis un browser : http://192.168.32.24:9200/_cat/indices

Resultat

| Status | Index Name                                              | Index ID               | Docs | Shards | Pri Docs | Pri Store | Size    | Pri Size |
| ------ | ------------------------------------------------------- | ---------------------- | ---- | ------ | -------- | --------- | ------- | -------- |
| green  | datahub-material-20230728                               | dgNFKGmfTWqDNFldPin6rw | 1    | 1      | 0        | 0         | 416b    | 208b     |
| green  | dh-collection_event
| green  | dh-coordinates
| green  | dh-geolocation
| green  | dh-identification
| green  | dh-material
| green  | dh-taxon
| green  | dh-taxon
| green  | material-generic
| green  | material-generic
| green  | material
| green  | material
| green  | material
| green  | ref-taxon-taxref-20221011103110                         | zUbksZA1TNOGSJgCIcilWQ | 1    | 1      | 657609   | 0         | 315mb   | 157.5mb  |
| green  | ref-taxon-taxref-20221011115619                         | bjPayENqR9CG_5jJ0vRY2w | 1    | 1      | 657609   | 0         | 331.3mb | 165.6mb  |
| green  | ref-taxon-taxref-20221026154110                         | 5GO0DaixS4u6SsIRSY-_Sg | 1    | 1      | 657609   | 0         | 334.9mb | 167.4mb  |
| green  | taxon                                                   | JrLm-8LLQkG5at9VTRFmHA | 1    | 1      | 1716     | 0         | 1.3mb   | 707.4kb  |
| green  | taxon_gbif_scientificname                               | 2s-PiRrxR421RiHO4QOY_w | 1    | 1      | 1716     | 0         | 1.2mb   | 614.5kb  |
| green  | taxon_ref_gbif                                          | RI0lMOKGSBOJlmF4ukBNbA | 1    | 1      | 6957236  | 0         | 7.2gb   | 3.5gb    |
| green  | taxon_ref_taxref                                        | DikmmicoQqmXz-_MA2Jw7A | 1    | 1      | 657609   | 0         | 686.4mb | 345.2mb  |
| green  | test_taxon_ref_col                                      | 9VmapHqSRK6eX-fdkuQK7A | 1    | 1      | 4706130  | 0         | 5.9gb   | 2.9gb    |


### Requêtes exemples

Extraire les 20 premiers docs de l'index dh-collection_event
```
curl -X GET 'http://192.168.32.24:9200/taxon_ref_gbif/_search?size=100'  -H 'Content-Type: application/json' -d '
{
    "query" : {
        "match_all" : {}
    }
}'
```

## Mapping RDF

Cf tableaux dans le dossier partagé sur le sharepoint MNHN : 
- chaque concept C a une liste d'attributs mappés dans un tableau C_attributes, eg pour le concept Identification => identification_attributes.xlsx. Ces tableaux listent les data properties (> litéraux). Ils sont disponibles dans [le dossier `Règles de migration - Attributs` du sharepoint](https://mnhnfr.sharepoint.com/sites/Pr-cadrageDataHub/Documents%20partages/Forms/AllItems.aspx?id=%2Fsites%2FPr%2DcadrageDataHub%2FDocuments%20partages%2F02%20%2D%20Realisation%2FMigration%20BU%2FConception%20fonctionnelle%2FR%C3%A8gles%20de%20migration%20%2D%20Attributs&viewid=f1a910a1%2Dc097%2D440a%2D87db%2D7ce75264d627)
- le mapping des principaux concepts et des objects properties est dans le tableau [`Mapping RDF - Modèle de données hors attributs`](https://mnhnfr.sharepoint.com/:x:/r/sites/Pr-cadrageDataHub/_layouts/15/Doc.aspx?sourcedoc=%7B96EDD591-BC72-4997-B853-75C0DE697D19%7D&file=Mapping%20RDF%20-%20Mod%C3%A8le%20de%20donn%C3%A9es%20hors%20attributs.xlsx&action=default&mobileredirect=true)
- le modèle graphML donne enfin une vue d'ensemble des liens entre les instances de classes avec, pour chaque classe toutes les dataProperties qui lui sont liées (en tant que rdfs:domain) https://datahub-models-mnhn-fr-datahub-8247b4bc9cc241b53c5db44d625a61dd.gitlab.io/ 
- WIP : exemple de RDF cible à générer pour chaque index parsé => base-nat_mapping-RDF.trig
  