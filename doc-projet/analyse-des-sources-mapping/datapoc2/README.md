
# Datapoc 2

##  Calames (oai-pmh) 

Les données de CALAMES peuvent être récupérées via le protocole OAI-PMH. La liste des notices du museum est donnée ici [ListSets](https://bibliotheques.mnhn.fr/EXPLOITATION/Infodoc/oaiserver.ashx?verb=ListSets).

Il faut retrouver la liste des identifiants afin de moissonner les documents par identifiants. Selon la documentation , il existe deux requêtes qui retourne les identifiants, ListIdentifiers et ListRecords.

Les sets de CALAMES peuvent correspondre aux sets suivants mnhn:manuscrits, mnhn:objets_art. En revanche, il est dit que pour récupérer toutes les entrées (records)
il est recommandé d'utiliser les requêtes ListRecords ou ListIdentifiers sans l'argument netSpec.



### Calames endpoint OAI-PMH

[Manuel des informaticiens](http://documentation.abes.fr/aidecalames/manuelinformaticien/index.html#MoissonnageOaiPmh_1)

| Requête  | URL  |
|---|---|
| endpoint (base url) |http://www.calames.abes.fr/oai/oai2.aspx |
| ListIdentifiers depuis une date | http://www.calames.abes.fr/oai/oai2.aspx?verb=ListIdentifiers&metadataPrefix=oai_dc&from=2012-05-01  |
| ListIdentifiers compris entre deux dates et dans un set  |  http://www.calames.abes.fr/oai/oai2.aspx?verb=ListRecords&metadataPrefix=oai_dc&from=2011-05-22&until=2012-05-23&set=920509801 |
|  Pagination  |  http://www.calames.abes.fr/oai/oai2.aspx?verb=ListRecords&resumptionToken =jeton à compléter | 


[Requête Identify Result](https://bibliotheques.mnhn.fr/EXPLOITATION/Infodoc/oaiserver.ashx?verb=Identify)

```xml
<?xml version="1.0" encoding="utf-8"?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
    <responseDate>2022-01-05T14:58:01Z</responseDate>
    <request verb="Identify">bibliotheques.mnhn.fr/EXPLOITATION/Infodoc/oaiserver.ashx</request>
    <Identify>
        <repositoryName>Bibliotheques du Muséum National d'Histoire Naturelle</repositoryName>
        <baseURL>http://localhost/EXPLOITATION/oaiserver.ashx</baseURL>
        <protocolVersion>2.0</protocolVersion>
        <adminEmail>thomas.orlean@mnhn.fr</adminEmail>
        <earliestDatestamp></earliestDatestamp>
        <deletedRecord>transient</deletedRecord>
        <granularity>YYYY-MM-DDTHH:MM:SS</granularity>
        <description>
            <oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
                <dc:description xml:lang="fre">Cet entrepôt OAI a été mis en place par la la Direction des Bibliothèques et de la Documentation du Muséum National d'Histoire Naturelle (Paris). Il permet de moissonner toutes les notices des documents consultables dans les collections numérisées des biliothèques du Muséum, spécialisée en sciences naturelles, botanique, zoologie, géologie, paléontologie, préhistoire, écologie, sciences de l'environnement... (http://bibliotheques.mnhn.fr)</dc:description>
            </oai_dc:dc>
        </description>
    </Identify>
</OAI-PMH>
```
### Echantillonage  des données MNHN dans Calames

**Liens**
* un exemple : http://www.calames.abes.fr/oai/oai2.aspx?verb=GetRecord&metadataPrefix=oai_ead&identifier=oai:oaicalames.abes.fr:FileId-1087
* liste des ID : -
* doc : http://documentation.abes.fr/aidecalames/manuelinformaticien/index.html#MoissonnageOaiPmh_1
* ex d'ID : `oai:oaicalames.abes.fr:FileId-532`

**Paramètres**

* `metadataPrefix` : au choix entre  `oai_ead` et `oai_dc`
* `set` : code RCR de l'établissement; pour le MNHN c'est 751059807

**Récupérer l'ensemble des données MNHN**
* Il faut moissoner en utilisant le "resumption token" dispo à la fin de chaque réponse:
* On peut les récup en EAD ou DublinCore en changeant le `metadataPrefix`
* URL pour scroller dans l'ensembles des "records" : http://www.calames.abes.fr/oai/oai2.aspx?verb=ListRecords&metadataPrefix=oai_ead&set=751059807
* URL pour scroller dans l'ensembles des "identifiers" uniquement de ces "records" : http://www.calames.abes.fr/oai/oai2.aspx?verb=ListIdentifiers&metadataPrefix=oai_ead&set=751059807


### Recherche d'un fond en particulier: correspondance de Gabriel Bertrand

Récupérer le  "fonds de correspondance de Gabriel Bertrand, sous les cotes Ms 3388 à Ms 3392. Cet ensemble comprend donc plusieurs correspondances : entrée auteur, mais aussi destinataire de lettres, auteur supposé, sujet. Ainsi que des auteurs existants dans IDRef mais pas tous."
* en cherchant ce fond sur l'UI web, on a récupère l'ID dans Calames : `Calames-2020924991147225`. Cet ID par contre semble correspondre uniquement au données DublinCore. J'ai réussi à recomposer l'id en ajoutant `FileId-` entre le prefix `oai:oaicalames.abes.fr:` et l'id de l'URL 
* => URL pour l'appel OAI de ce fonds en DublinCore (`oai_dc`) : 
  * http://www.calames.abes.fr/oai/oai2.aspx?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:oaicalames.abes.fr:FileId-Calames-2020924991147225

#### Comparaison d'une même notice en EAD et DC

Extrait en XML de la notice correspondant à cet élément du catalogue : 
http://www.calames.abes.fr/pub/mnhn.aspx#details?id=Calames-20191161511461692

**Note** :  

* Au format EAD on n'accède pas directement à une notice d'un élément de catalogue, mais à des niveaux hiérarchiques plus élevés. Ainsi pour retrouver la notice EAD correspondante à la page ci-dessus du catalogues Calames, j'ai du la rechercher avec la date de l'enregistrement 

* le gros avantage de la forme EAD est que nous récupérons les ID des personnes sur IDref quand elles existent, ainsi que leur rôle encodé de manière plus précise. Par exemple dans les exctraits ci-après on récupére les ID idref de :
  * Emil Abderhalden http://www.idref.fr/050148702, en tant qu'auteur (role="070", voir doc Abes (Listes fermées de valeurs d'attributs employées dans Calames)[http://documentation.abes.fr/aidecalames/manueloutildecatalogage/#annexe2])
  * Gabriel Bertrand  http://www.idref.fr/073010901, en tant que sujet (role="sujet")



**Au format EAD**

Extrait du fichier ./données en exemple/calames/Calames_EAD_FileId-2055_one-record-only.xml 
```xml
<c id="Calames-20191161511461692">
    <did>
        <unitid type="division">1-3</unitid>
        <unittitle>
            <persname normal="Abderhalden, Émil (1877-1950)" source="Sudoc"
                authfilenumber="050148702" role="070">Emil Abderhalden</persname>
        </unittitle>
        <langmaterial>Lettres rédigées en <language langcode="ger">allemand
            </language>
        </langmaterial>
        <unitdate era="ce" calendar="gregorian" normal="19320320/19380511">20 mars
            1932-11 mai 1938 </unitdate>
        <physdesc>
            <physfacet type="support">papier</physfacet>
            <extent>2 lettres et 1 enveloppe</extent>
        </physdesc>
    </did>
    <scopecontent>
        <p>La lettre du 11 mai 1938 est adressée à <persname normal="Agulhon, H."
                role="660">H. Agulhon</persname>, trésorier du Comité du jubilé
            scientifique de <persname normal="Bertrand, Gabriel (1867-1962)"
                role="sujet" source="Sudoc" authfilenumber="073010901">Gabriel
                Bertrand</persname>.</p>
    </scopecontent>
</c>
```

**Au format DC**

Extrait du fichier ./données en exemple/calames/extrait-fonds-Gabriel-Bertrand_FileId-Calames-20191161511461692_dc.xml
```xml
<oai_dc:dc xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/
http://www.openarchives.org/OAI/2.0/oai_dc.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:dc="http://purl.org/dc/elements/1.1/">
    <dc:title>Emil Abderhalden</dc:title>
    <dc:relation>FR-751059807 [RCR établissement]</dc:relation>
    <dc:relation>Nouveaux manuscrits de la bibliothèque du Muséum National d'Histoire naturelle [Fonds ou collection]
    </dc:relation>
    <dc:creator>Emil Abderhalden</dc:creator>
    <dc:contributor>H. Agulhon</dc:contributor>
    <dc:subject>Gabriel Bertrand</dc:subject>
    <dc:format>papier</dc:format>
    <dc:format>2 lettres et 1 enveloppe</dc:format>
    <dc:date>19320320/19380511</dc:date>
    <dc:language>ger</dc:language>
    <dc:description>La lettre du 11 mai 1938 est adressée à H. Agulhon, trésorier du Comité du jubilé scientifique de
        Gabriel Bertrand.</dc:description>
    <dc:type>series</dc:type>

</oai_dc:dc>
```
## sciencepress.mnhn.fr (oai-pmh) 

Ces données sont disponible sur l'entrepot OAI-PMH. Il s'agit de données de publications.

### sciencepress.mnhn.fr endpoint OAI-PMH 

| Requête  | URL  |
|---|---|
| endpoint (base url) |https://sciencepress.mnhn.fr/oai |
| ListIdentifiers depuis une date | https://sciencepress.mnhn.fr/en/oai?verb=ListIdentifiers&metadataPrefix=oai_dc&from=2014-01-17T13:43:42Z |

Tester avec : metadataPrefix=oai_dc

[Requête Identify Result](https://sciencepress.mnhn.fr/oai?verb=Identify)

```xml
<?xml version="1.0" encoding="UTF-8"?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/   http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
    <responseDate>2022-01-05T15:27:02Z</responseDate>
    <request verb="Identify">https://sciencepress.mnhn.fr</request>
    <Identify>
        <repositoryName>Publications scientifiques du Muséum national d&#039;Histoire naturelle, Paris</repositoryName>
        <baseURL>https://sciencepress.mnhn.fr/oai</baseURL>
        <protocolVersion>2.0</protocolVersion>
        <adminEmail>no-reply@sciencepress.mnhn.fr</adminEmail>
        <earliestDatestamp>2014-01-17T13:43:42Z</earliestDatestamp>
        <deletedRecord>no</deletedRecord>
        <granularity>YYYY-MM-DDThh:mm:ssZ</granularity>
        <compression>gzip</compression>
    </Identify>
</OAI-PMH>
```

### Echantillonage de données Sciencepress sur l'entrepôt OAI

**URL**

Pour récupérer un record, on peut faire https://sciencepress.mnhn.fr/en/oai?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:sciencepress.mnhn.fr:14463
* prefix : https://sciencepress.mnhn.fr/en/oai 
* verb : `GetRecord`
* metadataPrefix: `oai_dc` => on récupère du DublinCore
* identifier: base = `oai:sciencepress.mnhn.fr:` + ID
oai:sciencepress.mnhn.fr:

**Exemple**

Extrait du fichier ./données en exemple/sciencepress/14463.xml, uniquement l'élément `<oai_dc:dc>`

```xml
<oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/  http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
    <dc:title>A propos de &amp;lt;i&amp;gt;Mallotus&amp;lt;/i&amp;gt; et
        &amp;lt;i&amp;gt;Deuteromallotus&amp;lt;/i&amp;gt; (&amp;lt;i&amp;gt;Euphorbiaceae&amp;lt;/i&amp;gt;) à
        Madagascar</dc:title>
    <dc:description>&lt;p&gt;L&amp;#39;auteur montre que le genre &lt;i&gt;Deuteromallotus&lt;/i&gt; est basé sur une
        mauvaise interprétation de certains caractères et doit être considéré comme synonyme de
        &lt;i&gt;Mallotus&lt;/i&gt;. Une nouvelle combinaison est établie, une nouvelle espèce est décrite, et une clé
        des espèces malgaches est proposée.&lt;/p&gt;</dc:description>
    <dc:description>&lt;p&gt;The genus &lt;i&gt;Deuteromallotus&lt;/i&gt; is shown to have been based on a misconception
        and its name to belong in the synonymy of &lt;i&gt;Mallotus&lt;/i&gt;, one new combination is made in that
        genus, a new species is described, and a key to the Madagascan species is provided.&lt;/p&gt;</dc:description>
    <dc:date>1995-12-15 00:00:00</dc:date>
    <dc:identifier>14463</dc:identifier>
    <dc:type>Article de fascicule</dc:type>
    <dc:creator>McPHERSON
        Gordon &lt;h3 class=&quot;field-label&quot;&gt;
        Auteur &lt;/h3&gt;

        McPHERSON</dc:creator>
    <dc:language>EN</dc:language>
    <dc:coverage>&lt;img
        src=&quot;https://sciencepress.mnhn.fr/sites/default/files/default_images/img-default_0.jpg&quot;
        alt=&quot;&quot; /&gt;</dc:coverage>
</oai_dc:dc>
```


##  Muscat

### Récupération en RDF par itération dans la liste des IDs

Les données Muscat peuvent être récupérées via l'API du Sudoc qui contiendrait 95% des données.
Il est possible de récupérer ces données en parcourant la liste des entrées liées au MNHN dans le SUDOC. François Mistral nous a fourni un export à la   date du 18/01/2022 disponible [à ce lien](./données en exemple/sudoc/identifiants-entrées-MNHN-dans-SUDOC.tsv)
  
Pour chaque identifiant, eg. 022311750, on peut accéder à un fichier RDF comprenant la notice du document, e.g [données en exemple/sudoc/022311750.rdf](données en exemple/sudoc/022311750.rdf)

Extrait du RDF pour cet identifiant http://www.sudoc.fr/022311750.rdf

```xml
<bibo:Book rdf:about="http://www.sudoc.fr/022311750/id">
        <!--content form (isbd), media type (rda)-->
        <isbd:P1001>
            <skos:Concept rdf:about="http://iflastandards.info/ns/isbd/terms/contentform/T1009">
                <skos:prefLabel xml:lang="en">text</skos:prefLabel>
            </skos:Concept>
        </isbd:P1001>
        <!--rda:content type-->
        <rdau:P60049>
            <skos:Concept rdf:about="http://rdaregistry.info/termList/RDAContentType/1020">
                <skos:prefLabel xml:lang="en">text</skos:prefLabel>
            </skos:Concept>
        </rdau:P60049>
        <dc:type>Text</dc:type>
        <!--media type-->
        <isbd:P1003>
            <skos:Concept rdf:about="http://iflastandards.info/ns/isbd/terms/mediatype/T1010">
                <skos:prefLabel xml:lang="en">unmediated</skos:prefLabel>
            </skos:Concept>
        </isbd:P1003>
        <dcterms:language rdf:resource="http://lexvo.org/id/iso639-3/fra"></dcterms:language>
        <gn:countryCode>CH</gn:countryCode>
        <bibo:oclcnum>31373811</bibo:oclcnum>
        <dc:title>Les facteurs déterminant le sexe / [Signé: Emile Yung ...</dc:title>
        <!--rda elements obsolète : voir rdam:P30003-->
        <rdaelements:modeOfIssuance>
            <skos:Concept rdf:about="http://RDVocab.info/termLIst/ModeIssue/1001">
                <skos:prefLabel xml:lang="en">Single unit</skos:prefLabel>
            </skos:Concept>
        </rdaelements:modeOfIssuance>
        <!--mode of issuance-->
        <rdam:P30003>
            <skos:Concept rdf:about="http://RDVocab.info/termLIst/ModeIssue/1001">
                <skos:prefLabel xml:lang="en">Single unit</skos:prefLabel>
            </skos:Concept>
        </rdam:P30003>
        <dc:date>19 </dc:date>
        <dc:publisher>(Genève : impr. de P. Ritcher , s.d.)</dc:publisher>
        <dc:format>23 p. ; 8vo</dc:format>
        <skos:note>Extrait de la Revue de Morale Sociale, no 5, mars 1900</skos:note>
        <marcrel:aut>
            <foaf:Person rdf:about="http://www.idref.fr/117021369/id">
                <foaf:name>Yung, Emile (1854-1918)</foaf:name>
            </foaf:Person>
        </marcrel:aut>
    </bibo:Book>
```

### Extrait d'une liste de d'identifiant
TBD
```
1. 060835201, Muséum national d'histoire naturelle (Paris). Bibliothèque du Jardin botanique exotique (Menton, Alpes-Maritimes)
2. 241725201, Muséum national d'histoire naturelle (Paris). Bibliothèque de l'Abri Pataud (Les Eyzies-de-Tayac-Sireuil)
3. 751055233, Muséum national d'histoire naturelle (Paris). Bibliothèque d'écologie et de gestion de la biodiversité (CESCO et Patrinat)
4. 751055232, Muséum national d'histoire naturelle (Paris). Département des jardins botaniques et zoologiques. Bibliothèque de pédagogie
5. 751059902, Muséum national d'histoire naturelle (Paris). Bibliothèque électronique
6. 751055229, Muséum national d'histoire naturelle (Paris). Bibliothèque d'anatomie comparée
7. 751052337, Muséum national d'histoire naturelle (Paris). Département des milieux et peuplements aquatiques. Bibliothèque
8. 911145101, Muséum national d'histoire naturelle (Paris). Bibliothèque d'écologie générale (Brunoy, Essonne)
9. 751132301, Muséum national d'histoire naturelle (Paris). Institut de paléontologie humaine. Bibliothèque
10. 751122301, Parc zoologique (Paris). Bibliothèque
11. 751055215, Muséum national d'histoire naturelle (Paris). Bibliothèque de géologie
12. 751055214, Muséum national d'histoire naturelle (Paris). Bibliothèque de chimie
13. 751055209, Muséum national d'histoire naturelle (Paris). Bibliothèque des Jardins
14. 751055208, Muséum national d'histoire naturelle (Paris). Bibliothèque de biologie parasitaire
15. 751055206, Muséum national d'histoire naturelle (Paris). Bibliothèque d'ethnobiologie
16. 751055106, Muséum national d'histoire naturelle (Paris). Bibliothèque de zoologie-arthropodes
17. 751055104, Muséum national d'histoire naturelle (Paris). Bibliothèque de minéralogie
18. 751055103, Muséum national d'histoire naturelle (Paris). Bibliothèque de la Ménagerie du Jardin des Plantes
19. 751052328, Muséum national d'histoire naturelle (Paris). Bibliothèque d'anthropologie maritime
20. 751052313, Muséum national d'histoire naturelle (Paris). Bibliothèque de zoologie-reptiles et amphibiens
21. 751052312, Muséum national d'histoire naturelle (Paris). Bibliothèque de zoologie-mammifères et oiseaux
22. 751052310, Muséum national d'histoire naturelle (Paris). Bibliothèque de paléontologie
23. 751052309, Muséum national d'histoire naturelle (Paris). Bibliothèque de botanique
24. 751052308, Muséum national d'histoire naturelle (Paris). Bibliothèque de malacologie
25. 751052306, Muséum national d'histoire naturelle (Paris). Bibliothèque d'ichtyologie
26. 751052305, Muséum national d'histoire naturelle (Paris). Bibliothèque d'entomologie
27. 751052304, Muséum national d'histoire naturelle (Paris). Bibliothèque centrale
```

