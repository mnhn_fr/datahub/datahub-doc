# BHL - Documentation connecteur 

**Objectif :** récupérer les articles BHL mentionnant taxons ou specimen MNHN

## Prop / entity


## Doc API REST BHL

cf https://www.biodiversitylibrary.org/docs/api3/

### Get API key


### Use GetNameMetadata

cf [GetNameMetadata](https://www.biodiversitylibrary.org/docs/api3/GetNameMetadata.html) to retrieve list of publications for a given scientific name


Template call

```

https://www.biodiversitylibrary.org/api3?op=GetNameMetadata
    &name=<a complete scientific name (not used if id/idtype are specified)>
    &id=<identifier of a scientific name (not used if name is specified)>
    &idtype=<namebank, eol, gni, ion, col, gbif, itis, ipni, or worms (not used if name is specified)>
    &format=<"xml" for an XML response or "json" for JSON (OPTIONAL; "xml" is the default)>
    &apikey=<API key value>

```

API-key (mine) = `cfb561de-2ec4-4dfc-8837-c42041dc802d`

Example call with GBIF id (see below to retrieve it from MNHN Graph)

```
https://www.biodiversitylibrary.org/api3?op=GetNameMetadata&id=3578999&idtype=gbif&apikey=cfb561de-2ec4-4dfc-8837-c42041dc802d
```
 
Example call with name

```
https://www.biodiversitylibrary.org/api3?op=GetNameMetadata&name=Secamone falcata&apikey=cfb561de-2ec4-4dfc-8837-c42041dc802d
```

Voir un [exemple de Résultat XML](bhl-api3-example-GetNameMetadat_secamone-falcata.xml) 

Autres exemples d'appels:

```
https://www.biodiversitylibrary.org/api3?op=GetNameMetadata&name=Hieracium denticulatum&apikey=cfb561de-2ec4-4dfc-8837-c42041dc802d
https://www.biodiversitylibrary.org/api3?op=GetNameMetadata&name=Chloris pilosa&apikey=cfb561de-2ec4-4dfc-8837-c42041dc802d
https://www.biodiversitylibrary.org/api3?op=GetNameMetadata&name=Agrostis atlantica var. typica&apikey=cfb561de-2ec4-4dfc-8837-c42041dc802d
```


NB : les résultats sont plus restreints avec les ID qu'avec les noms scientifiques

## API OAI PMH


metadataPrefix values : 
* oai_dc
* mods
* olef

Exemple d'appel https://www.biodiversitylibrary.org/oai?verb=ListRecords&metadataPrefix=mods ([Voir résultats](bhl-list-records-OAI-MODS.xml))  

**NB** le resumption token n'est pas bien formé ! `<resumptionToken>|||mods|item:897|2024-09-30T04:14:53Z</resumptionToken>`

## Récupération des taxons MNHN



Les taxons MNHN sont intégrés dans le graphe MNHN. Pour éviter les ambiguités ou les problèmes de variabilités des formes (accents, ponctuation, avec ou sans autorité, etc.) liées aux noms scientifiques, il est possible de récupérer les alignements vers les référentiels tels que GBIF, TaxREF, IPNI, etc.

```sparql
PREFIX mnhn: <https://www.data.mnhn.fr/ontology/>
PREFIX dwc: <http://rs.tdwg.org/dwc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

select *
where { 
	?t a mnhn:Taxon;
    	dwc:scientificName ?scName;
     	mnhn:scientificNameWOAuthorship ?scNamewo.
	#Get GBIF id if any	
      #OPTIONAL{
    ?t mnhn:hasTaxonAlignment/mnhn:taxonAlignedTo ?tn.
    ?tn skos:inScheme <https://www.gbif.org/species/>;
        dwc:taxonID ?GbifId.
     #}
} limit 100 
```

10 premiers résultats 

| t                                                | scName                                             | scNamewo                         | tn                                   | GbifId  |
| :----------------------------------------------- | :------------------------------------------------- | :------------------------------- | :----------------------------------- | :------ |
| mnhnd:taxon/002b6e10-b220-5e12-b361-770161e0802b | Sakoanala villosa R.Vig. subsp. villosa            | Sakoanala villosa subsp. villosa | https://www.gbif.org/species/7068560 | 7068560 |
| mnhnd:taxon/003046a4-a348-5641-895f-8ebbe11bfd9e | Secamone falcata Klack.                            | Secamone falcata                 | https://www.gbif.org/species/3578999 | 3578999 |
| mnhnd:taxon/0034fd93-c448-5794-8dc8-f3701fa882a9 | Chloris pilosa Schumach.                           | Chloris pilosa                   | https://www.gbif.org/species/3219738 | 3219738 |
| mnhnd:taxon/003ee983-4bbd-526f-bb96-507b0249135e | Senecio solanoides Asch.                           | Senecio solanoides               | https://www.gbif.org/species/3110426 | 3110426 |
| mnhnd:taxon/0047c7d2-197e-5ce2-9a8b-033af1c54946 | Hieracium denticulatum Jord.                       | Hieracium denticulatum           | https://www.gbif.org/species/4225066 | 4225066 |
| mnhnd:taxon/0051d7ce-bef9-55d0-9cb2-4d8c8ed17c53 | Agrostis atlantica Maire & Trab. var. typica Maire | Agrostis atlantica var. typica   | https://www.gbif.org/species/6292342 | 6292342 |


