Doc du projet Datahub - MNHN - Mnemotix  
=======================================

**Sommaire**

* [Analyse des sources (WIP)]()
    - [Bases unifiées](analyse-des-sources/bases-nat-unifiee/README.md)
    - [hal](analyse-des-sources/hal/README.md)
    - [gbif](analyse-des-sources/gbif/README.md)
    - [datapoc2](analyse-des-sources/datapoc2/README.md)
* [Doc modèle pivot (TBD)](modelisation/README.md) 
* [Doc Architecture Technique (DAT)(TBD)](architecture/README.md)  
* [Doc API (TBD)]()


**Analyse des données**

  
**NB** : Voir [tableau de synthèse](https://docs.google.com/spreadsheets/d/1i4b4FO2XU1Gq-6eIZ2MHsdfoETFu6Bzd-o01ATT3KRc/edit#gid=0) affiché ci-dessous

<iframe width="100%" height="600px"  src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQpw0DhC7_lt6Hs1Udc_UEPbW5T_Su2SxsR6hJBYtki-IJsH2vaX1z0INNeC4T5DyogjNS3SSjlOsqZ/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>

**Liens utiles**

* Dossier partagé Google Drive : [https://drive.google.com/drive/folders/1CaY4MDpYiz1gdj0whzkRynH5zMv7Kvyd](https://drive.google.com/drive/folders/1CaY4MDpYiz1gdj0whzkRynH5zMv7Kvyd)
