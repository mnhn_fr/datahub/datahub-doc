# DISSCO Project   

* Projet européen d'infrastucture de collecte de partage de données de specimen biologique
* Propose spec [OpenDS](https://github.com/DiSSCo/openDS) pour la description de specimens en open data et suivant les principes FAIR
* OpenDS se découpe en 3 composants:
  1. un modèle de données (cf [data model](https://github.com/DiSSCo/openDS/blob/master/data-model/data-model-intro.md)) basé sur PROV pour les éléments abstraits (entité, activité, agent, etc) 
  2. une ontology OWL 
  3. une définition d'API, en cours de définition, compatible avec le [protocole DOIP](https://hdl.handle.net/0.DOIP/DOIPV2.0)

